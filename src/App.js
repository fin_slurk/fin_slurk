import './App.css';
import locations from './locations.json';
import PlayerProfile from './Profiles/EntityProfile.js';
import Fight from './Combat/Fight';
import React, { useState } from 'react';
import Dialog from './NPC/Dialog';

//state variabel is een variabel die het hele component opnieuw laad. 

function App() {
  const [currentLocationId, setCurrentLocation] = useState("home");             // setCurrentLocation sets currentLocationId, "home is standaard waarde"
  const [currentNpcId, setCurrentNpc] = useState(null);
  const [isFighting, setIsFighting] = useState(false);
  const [currentEnemyId, setCurrentEnemyId] = useState(false);
  
  const player = {
    name: "Engelbert",
    location: currentLocationId,                                                //krijgt waarde van setCurrentLocation
    level: 1,
    totalHealth: 500,
    currentHealth: 500,                                                         //currentHealth kan aangepast worden door hele document
  };

  function travel(destination) {                                                // Van locatie wisselen door "useState" 
    setCurrentLocation(destination);
  }

  function talkTo(npc) {
    setCurrentNpc(npc);
  }


  function train(currentEnemyId) {
    setIsFighting(true);
    setCurrentEnemyId(currentEnemyId);
  }

  const currentLocationInformation = locations[currentLocationId];

  if (isFighting) {
    return (
      <div className="App">
        <header className="App-header">
          <Fight player={player} enemyId={currentEnemyId} />
        </header>
      </div>
    );
  }

  if (currentNpcId) {
    return (
      <div className="App">
        <header className="App-header">
          <Dialog currentNpcId={currentNpcId} train={train} talkTo={talkTo} />
        </header>
      </div>
    );
  }

  return (
    <div className="App">
      <header className="App-header">
          <img src={process.env.PUBLIC_URL + "/Images/Scene/" + currentLocationInformation.backgroundImage} className="backgroundImageLocation" />
          <div className="npcContainer">
            {currentLocationInformation.npcs.map((npc) =>
              <button onClick={(() => talkTo(npc))} type="button"><img src={process.env.PUBLIC_URL + "/Images/NPC/" + npc + "/default.jpg"} className="npcLocation" /></button>
            )}
          </div>

          <PlayerProfile player={player} />
          {currentLocationInformation.name}
          <br />
          {currentLocationInformation.description}
          <br />
          {currentLocationInformation.possibleDestinations.map((destination) =>
            <button onClick={(() => travel(destination))} type="button">{destination}</button>
          )}
      </header>
    </div>
  );
}

export default App;