import React from 'react';
import './Fight.css';
import EntityProfile from '../Profiles/EntityProfile';
import npcs from '../NPC/npcs.json';

function Fight({ player, enemyId }) {
    const enemy = npcs[enemyId];

    enemy.currentHealth = enemy.totalHealth;
    enemy.currentHealth = enemy.currentHealth - 470;

    return (
        <div className="fightContainer">
            <div className="playerCombatScreen"> 
            <img src={process.env.PUBLIC_URL + "/Images/Player/default.png"} className="npcLocation" />
            <EntityProfile player={player} /></div>
            <div className="enemyCombatScreen">
                <EntityProfile player={enemy} />
                <img src={process.env.PUBLIC_URL + "/Images/NPC/" + enemyId + "/default.jpg"} className="npcLocation" />
            </div>
        </div>
    );
}

export default Fight;