import React from 'react';
import './Dialog.css';
import npcs from './npcs.json';

function Dialog({ currentNpcId, train, talkTo }) {

    function pickRandomFromList(list) {
        return list[Math.floor(Math.random() * list.length)];
    }

    console.log(currentNpcId);
    const currentNpcInformation = npcs[currentNpcId];

    return (
        <div className="npcDialog">
            <img src={process.env.PUBLIC_URL + "/Images/NPC/" + currentNpcId + "/default.jpg"} className="npcLocation" />
            {currentNpcInformation.name}
            <br />
            {pickRandomFromList(currentNpcInformation.catchPhrases)}
            <br />
            <button onClick={(() => train(currentNpcId))} type="button">train</button>
            <button onClick={(() => talkTo())} type="button">goodbye</button>
        </div>
    );
}

export default Dialog;