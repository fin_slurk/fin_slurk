import React from 'react';
import './HealthBar.css';

function HealthBar({entity}) {
    const healthPercentage = (entity.currentHealth/entity.totalHealth)*100;
    
    return (
        <div>
            <div className={"health"}>
                <div className={"healthProgress"} style={{ width: healthPercentage + "%" }} />
                {entity.currentHealth}/{entity.totalHealth}

            </div>
        </div>
    );
}

export default HealthBar;