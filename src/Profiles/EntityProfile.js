import React from 'react';
import HealthBar from './HealthBar';
import './EntityProfile.css';

function EntityProfile({ player }) {

    return (
        <div>

            {player.name}
            <br />
            level {player.level}
            <HealthBar entity={player} />
            <br />
        </div>
    );
}

export default EntityProfile;